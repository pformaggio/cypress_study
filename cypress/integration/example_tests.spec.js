/// <reference types="Cypress" />


describe('Examples', function() {

    const THREE_SECONDS_IN_MILISECONDS = 3000

    beforeEach(() => {
        cy.visit('./src/index.html')
    })

    it('verifica o título da aplicação', function() {
        cy.title().should('be.equal','Examples')
    })

    it('Formulario', function(){
        const longText = 'TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTE'
        
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('teste@teste.com')
        cy.get('#phone').type('14999999999')
        cy.get('#open-text-area').type(longText, {delay: 0})
        cy.get('button[type="submit"]').click()
        cy.get('span[class="success"]').should('be.visible')
    })

    it('Mensagem de erro', function(){
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('pribelform')
        cy.get('#phone').type('149999999999')
        cy.get('#open-text-area').type('email errado')
        cy.get('button[type="submit"]').click()
        cy.get('span[class="error"]').should('be.visible')
    })

    it('Assert', function(){
        cy.get('#phone')
            .type('abd')
                .should('have.value', '')
    })

    it('Assert 2', function(){
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('teste@teste.com')
        cy.get('#phone-checkbox').click()
        cy.get('#open-text-area').type('email errado')
        cy.get('button[type="submit"]').click()
        cy.get('span[class="error"]').should('be.visible')
    })

    it('Limpa os campos', function(){
        cy.get('#firstName').type('Priscila').should('have.value', 'Priscila').clear().should('have.value', '')
        cy.get('#lastName').type('Formaggio').should('have.value', 'Formaggio').clear().should('have.value', '')
        cy.get('#email').type('teste@teste.com').should('have.value', 'teste@teste.com').clear().should('have.value', '')
        cy.get('#phone').type('14999999999').should('have.value', '14999999999').clear().should('have.value', '')
    })

    it('exibe mensagem de erro ao submeter o formulário sem preencher os campos obrigatórios', function(){
        cy.get('button[type="submit"]').click()
        cy.get('span[class="error"]').should('be.visible')
    })

    it('envia o formuário com sucesso usando um comando customizado', function(){
        const user = {
            nome: 'Priscila',
            sobrenome: 'Formaggio',
            email: 'teste@teste.com'
        }
        cy.fillMandatoryFieldsAndSubmit(user.nome, user.sobrenome, user.email, user.telefone)
    })

    it('cy.contains', function(){
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('teste@teste.com')
        cy.get('#phone').type('14999999999')
        cy.get('#open-text-area').type('longText')
        cy.contains('button', 'Enviar').click()
        cy.get('span[class="success"]').should('be.visible')
    })

    it('seleciona um produto (YouTube) por seu texto', function(){
        cy.get('#product').select('YouTube').should('have.value', 'youtube')
    })

    it('seleciona um produto (Mentoria) por seu valor', function(){
        cy.get('#product').select('mentoria').should('have.value', 'mentoria')
    })

    it('seleciona um produto (Blog) pelo seu índice', function(){
        cy.get('#product').select(1).should('have.value', 'blog')
    })

    it('marca o tipo de atendimento "Feedback"', function(){
        cy.get('input[type="radio"][value="feedback"]')
    })

    it('marca cada tipo de atendimento', function(){
        cy.get('input[type="radio"]')
            .should('have.length', 3)
                .each(function($radio){
                    cy.wrap($radio).check()
                    cy.wrap($radio).should('be.checked')
                })
    })

    it('marca ambos checkboxes, depois desmarca o último', function(){
        cy.get('input[type="checkbox"]')
            .check()
                .last()
                    .uncheck()
                        .should('not.be.checked')
           
    })

    it('exibe mensagem de erro quando o telefone se torna obrigatório mas não é preenchido antes do envio', function(){
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('teste@teste.com')
        cy.get('#phone-checkbox').check().should('be.checked')
        cy.get('#open-text-area').type('email errado')
        cy.get('button[type="submit"]').click()
        cy.get('span[class="error"]').should('be.visible')
           
    })

    it('seleciona um arquivo da pasta fixtures', function(){
        cy.get('input[type="file"]').selectFile('./cypress/fixtures/example.json')
            .should(function($input){
                expect($input[0].files[0].name).to.equal('example.json')
            })
    })

    it('seleciona um arquivo simulando um drag-and-drop', function(){
        cy.get('input[type="file"]').selectFile('./cypress/fixtures/example.json', {action: "drag-drop"})
            .should(function($input){
                expect($input[0].files[0].name).to.equal('example.json')
            })
    })

    it('seleciona um arquivo utilizando uma fixture para a qual foi dada um alias', function(){
        cy.fixture('example.json').as('sampleFile')
        cy.get('input[type="file"]').selectFile('@sampleFile')
            .should(function($input){
                expect($input[0].files[0].name).to.equal('example.json')
            })
    })

    it('verifica que a política de privacidade abre em outra aba sem a necessidade de um clique', function(){
        cy.get('#privacy a').should('have.attr', 'target', '_blank')
    })

    it('acessa a página da política de privacidade removendo o target e então clicanco no link', function(){
        cy.get('#privacy a').invoke('removeAttr', 'target').click()
        cy.contains('Talking About Testing')
    })


    it('preenche os campos obrigatórios e envia o formulário', function(){
        const longText = 'TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTE'
        cy.clock()
        cy.get('#firstName').type('Priscila')
        cy.get('#lastName').type('Formaggio')
        cy.get('#email').type('teste@teste.com')
        cy.get('#phone').type('14999999999')
        cy.get('#open-text-area').type(longText, {delay: 0})
        cy.get('button[type="submit"]').click()
        cy.get('span[class="success"]').should('be.visible')
        cy.tick(THREE_SECONDS_IN_MILISECONDS)
        cy.get('span[class="success"]').should('not.be.visible')        
    })

    // Lodash - Cypress.times
    Cypress._.times(3, function(){
        it('preenche os campos obrigatórios e envia o formulário', function(){
            const longText = 'TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTETESTE, TESTE, TESTE, TESTE, TESTE'
            cy.clock()
            cy.get('#firstName').type('Priscila')
            cy.get('#lastName').type('Formaggio')
            cy.get('#email').type('teste@teste.com')
            cy.get('#phone').type('14999999999')
            cy.get('#open-text-area').type(longText, {delay: 0})
            cy.get('button[type="submit"]').click()
            cy.get('span[class="success"]').should('be.visible')
            cy.tick(THREE_SECONDS_IN_MILISECONDS)
            cy.get('span[class="success"]').should('not.be.visible')        
        })
    })

    //  Lodash - Cypress.times
    it('exibe e esconde as mensagens de sucesso e erro usando o .invoke()', function(){
        cy.get('.success')
        .should('not.be.visible')
        .invoke('show')
        .should('be.visible')
        .and('contain', 'Mensagem enviada com sucesso.')
        .invoke('hide')
        .should('not.be.visible')
        cy.get('.error')
        .should('not.be.visible')
        .invoke('show')
        .should('be.visible')
        .and('contain', 'Valide os campos obrigatórios!')
        .invoke('hide')
        .should('not.be.visible')   
    })
    
    
    it('preenche a area de texto usando o comando invoke', function(){
        const longText = Cypress._.repeat('0123456789', 20)

        cy.get('#open-text-area')
            .invoke('val', longText)
            .should('have.value', longText)
    })

    
    it('faz uma requisição HTTP', function(){
        cy.request('https:teste/index.html')
            .should(function(response){
                const {status, statusText, body} = response 
                expect(status).to.equal(200)
                expect(statusText).to.equal('OK')
                expect(body).to.include('CAC TAT')
            })
    })


})
  