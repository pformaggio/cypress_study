import { faker } from "@faker-js/faker";
import * as cpf from "@fnando/cpf";
import * as cnpj from "@fnando/cnpj";

describe("Dado que estou logado como admin user", () => {
  beforeEach(() => {
    cy.visit();
    cy.login("teste@teste.com");
  });

  it("Então crio um novo usuário investidor e o login é validado com sucesso", () => {
    const userData = {
      name: faker.name.fullName(),
      email: faker.internet.email().toLowerCase(),
      taxpayerIdentificationNumber: cpf.generate(),
    };

   cy.fillMandatoryFieldsAndSubmit(userData);
  });

  it("Então eu consigo criar um usuário originador e atribuir um usuário com sucesso", () => {
    const userData = {
      name: faker.name.fullName(),
      email: faker.internet.email().toLowerCase(),
    };

    cy.fillMandatoryFieldsAndSubmit(userData);
  });
});